class Figure {
    constructor(cls) {
        this.cls = cls
    }
}


class GameBoard {
    constructor(width, height, style) {
        this.width = width
        this.height = height
        this.style = style
        this.cells = [
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0]
        ]
        this.figures = [
            [2, 3, 4, 5, 6, 4, 3, 2],
        	[1, 1, 1, 1, 1, 1, 1, 1],
        	[0, 0, 0, 0, 0, 0, 0, 0],
        	[0, 0, 0, 0, 0, 0, 0, 0],
        	[0, 0, 0, 0, 0, 0, 0, 0],
        	[0, 0, 0, 0, 0, 0, 0, 0],
        	[7, 7, 7, 7, 7, 7, 7, 7],
        	[8, 0, 0, 0, 0, 0, 0, 8]
        ]
        this.board = document.getElementById('game-board')
    }

    createCells(type) {
        let cell = document.createElement('div')
        cell.classList.add(type)
        this.board.append(cell)
    }



    renderCells() {
        this.cells.forEach((lines) => {
            lines.forEach((c) => {
                if(c === 1) {
                    this.createCells('cell-white')
                }
                else {
                    this.createCells('cell-black')
                }
            })
        })
    }


    startGame() {
        this.renderCells()
    }
}

board = new GameBoard(1024, 768, {color: 'red', background: '#ccc'})


board.startGame()


