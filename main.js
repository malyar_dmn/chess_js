const cells = [
	[0, 1, 0, 1, 0, 1, 0, 1],
	[1, 0, 1, 0, 1, 0, 1, 0],
	[0, 1, 0, 1, 0, 1, 0, 1],
	[1, 0, 1, 0, 1, 0, 1, 0],
	[0, 1, 0, 1, 0, 1, 0, 1],
	[1, 0, 1, 0, 1, 0, 1, 0],
	[0, 1, 0, 1, 0, 1, 0, 1],
	[1, 0, 1, 0, 1, 0, 1, 0]
]



// const figuresPos = [
// 	[2, 3, 4, 5, 6, 4, 3, 2],
// 	[1, 1, 1, 1, 1, 1, 1, 1],
// 	[0, 0, 0, 0, 0, 0, 0, 0],
// 	[0, 0, 0, 0, 0, 0, 0, 0],
// 	[0, 0, 0, 0, 0, 0, 0, 0],
// 	[0, 0, 0, 0, 0, 0, 0, 0],
// 	[7, 7, 7, 7, 7, 7, 7, 7],
// 	[8, 0, 0, 0, 0, 0, 0, 8]
// ]

const figuresPos = [
	['br', 'bk', 'bb', 'bq', 'bKing', 'bb', 'bk', 'br'],
	['bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp'],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	['wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp'],
	['wr', 'wk', 'wb', 'wq', 'wKing', 'wb', 'wk', 'wr']
]

let board = document.querySelector('#game-board')
let figuresBoard = document.getElementById('game-board-figures')

let moveFigure = (e) => {
	console.log(e)
}

let createCells = (type) => {
	let cell = document.createElement('div')
	cell.classList.add(type)
	board.append(cell)
}

let createFigure = (figureType) => {
	let figure = document.createElement('div')
	figure.classList.add(figureType)
	figuresBoard.append(figure)
}



	
let renderCells = () => {
	cells.forEach((lines) => {
		lines.forEach((c) => {
			if(c === 1) {
				createCells('cell-white')
			}
			else {
				createCells('cell-black')
			}
		})
	})
}


let renderFigures = () => {
	figuresPos.forEach((ln) => {
		ln.forEach((figure) => {
			console.log(figure)
			if(figure === 'bp') {
				createFigure('blackPawn')
			}
			else if(figure === 'br') {
				createFigure('blackRook')
			}
			else if(figure === 'bk') {
				createFigure('blackKnight')
			}
			else if(figure === 'bb') {
				createFigure('blackBishop')
			}
			else if(figure === 'bq') {
				createFigure('blackQueen')
			}
			else if(figure === 'bKing') {
				createFigure('blackKing')
			}
			else if(figure === 'wp') {
				createFigure('whitePawn')
			}
			else {
				createFigure('empty')
			}
		})
	})
}

renderCells()
renderFigures()